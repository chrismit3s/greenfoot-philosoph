import greenfoot.*;


public class Spoon extends Actor {
	public static final int SPOON_LEN = 96;


	private final int homeX, homeY, homeAngle;
	private Philo owner = null;

	public Spoon(int x, int y, int angle) {
		super();

		// set position and rotation
		this.homeX = x;
		this.homeY = y;
		this.homeAngle = angle;

		// create image
		this.updateImage();
	}

	public boolean isAvailable() {
		return this.owner == null;
	}

	public boolean isOwnedBy(Philo owner) {
		return this.owner == owner;
	}

	private void updateImage() {
		GreenfootImage img = null;
		img = new GreenfootImage("images/spoon.png");
		int width = img.getWidth();
		int height = img.getHeight();
		img.scale(SPOON_LEN, height * SPOON_LEN / width);
		this.setImage(img);
	}


	public boolean take(Philo owner) {
		if (this.isAvailable() || this.isOwnedBy(owner)) {
			this.owner = owner;
			this.setLocation((this.homeX + this.owner.getX()) / 2, (this.homeY + this.owner.getY()) / 2);
			this.turnTowards(this.homeX, this.homeY);
			return true;
		}
		else {
			return false;
		}
	}

	public void putBack() {
		this.owner = null;
		this.resetPosition();
	}

	public void resetPosition() {
		this.setLocation(this.homeX, this.homeY);
		this.setRotation(this.homeAngle);
	}
}

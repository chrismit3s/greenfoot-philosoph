# Dining philosophers

A small demo of the [Dining philosophers problem](https://en.wikipedia.org/wiki/Dining_philosophers_problem), a classic example of a (potential) deadlock.
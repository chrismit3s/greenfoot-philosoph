import greenfoot.*;


public class Philo extends Actor {
	public static final int PHILO_SIZE = 128;


	private final Spoon left, right;

	public Philo(Spoon left, Spoon right) {
		super();

		this.left = left;
		this.right = right;

		// create image
		this.updateImage();
	}

	public int numSpoons() {
		return (this.left.isOwnedBy(this) ? 1 : 0) + (this.right.isOwnedBy(this) ? 1 : 0);
	}

	public void select() {
		this.getWorldOfType(PhiloWorld.class).select(this);
		this.updateImage();
	}

	public boolean isSelected() {
		return (this.getWorld() == null) ? false : this.getWorldOfType(PhiloWorld.class).isSelected(this);
	}

	public void act() {
		if (Greenfoot.mouseClicked(this)) {
			this.clicked();
		}
	}

	public void clicked() {
		switch (this.numSpoons()) {
		case 0:
		case 1:
			this.select();
			break;
		case 2:
			this.stopEating();
			break;
		}
	}

	public void updateImage() {
		GreenfootImage img = null;

		int numSpoons = this.numSpoons();
		if (this.isSelected())
			img = new GreenfootImage("images/philo-highlight.png");
		else if (numSpoons == 0)
			img = new GreenfootImage("images/philo-think.png");
		else if (numSpoons == 1)
			img = new GreenfootImage("images/philo.png");
		else if (numSpoons == 2)
			img = new GreenfootImage("images/philo-eat.png");

		img.scale(PHILO_SIZE, PHILO_SIZE);
		this.setImage(img);
	}

	public boolean takeSpoon(Spoon spoon) {
		if (spoon == this.left)
			return this.takeLeft();
		else if (spoon == this.right)
			return this.takeRight();
		else
			return false;
	}

	public boolean takeLeft() {
		boolean ret = this.left.take(this);
		this.updateImage();
		return ret;
	}

	public boolean takeRight() {
		boolean ret = this.right.take(this);
		this.updateImage();
		return ret;
	}

	public void stopEating() {
		this.left.putBack();
		this.right.putBack();
		this.updateImage();
	}
}

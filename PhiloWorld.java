import greenfoot.*;
import java.util.*;


public class PhiloWorld extends World {
	public static final int WORLD_WIDTH = 1920, WORLD_HEIGHT = 1080;
	public static final int TABLE_SIZE = 512;
	public static final int NUM_PHILOS = 6;

	private final Spoon[] spoons = new Spoon[NUM_PHILOS];
	private final Philo[] philos = new Philo[NUM_PHILOS];
	private Philo selection = null;

	public PhiloWorld() {
		super(WORLD_WIDTH, WORLD_HEIGHT, 1);

		// create background
		GreenfootImage bg = this.getBackground();

		GreenfootImage table = new GreenfootImage("images/table.png");
		table.scale(TABLE_SIZE, TABLE_SIZE);

		bg.drawImage(table, (WORLD_WIDTH - TABLE_SIZE) / 2, (WORLD_HEIGHT - TABLE_SIZE) / 2);
		this.setBackground(bg);

		// create spoons
		int centerX = WORLD_WIDTH / 2;
		int centerY = WORLD_HEIGHT / 2;
		int radius = TABLE_SIZE / 2 - 2 * Spoon.SPOON_LEN / 3;
		for (int i = 0; i != NUM_PHILOS; ++i) {
			double angle = 360.0 * (i + 0.5) / NUM_PHILOS;  // the +0.5 offsets the spoons so they appear between the philos
			double x = centerX - radius * Math.cos(angle / 180 * Math.PI);
			double y = centerY - radius * Math.sin(angle / 180 * Math.PI);

			this.spoons[i] = new Spoon((int)x, (int)y, (int)angle);
			this.addObject(this.spoons[i], (int)x, (int)y);
			this.spoons[i].resetPosition();
		}

		// create philos
		radius = TABLE_SIZE / 2 + 2 * Philo.PHILO_SIZE / 3;
		for (int i = 0; i != NUM_PHILOS; ++i) {
			double angle = 360.0 * i / NUM_PHILOS;
			double x = centerX - radius * Math.cos(angle / 180 * Math.PI);
			double y = centerY - radius * Math.sin(angle / 180 * Math.PI);

			Spoon left = this.spoons[i];
			Spoon right = this.spoons[(i + NUM_PHILOS - 1) % NUM_PHILOS];
			this.philos[i] = new Philo(left, right);
			this.addObject(this.philos[i], (int)x, (int)y);
		}
	}

	public void act() {
		if (this.selection != null) {
			for (Spoon spoon : this.spoons) {
				if (!spoon.isAvailable() || !Greenfoot.mouseClicked(spoon))
					continue;

				// try to take spoon, if it succeeds, we clear the selection
				if (this.selection.takeSpoon(spoon))
					this.select(null);

				break;
			}
		}
	}

	public boolean isSelected(Philo philo) {
		return this.selection == philo;
	}

	public void select(Philo newSelection) {
		Philo oldSelection = this.selection;
		if (this.selection == newSelection)
			this.selection = null;
		else
			this.selection = newSelection;

		if (oldSelection != null)
			oldSelection.updateImage();
		if (newSelection != null)
			newSelection.updateImage();
	}
}
